#!/usr/bin/env sh

fasm ff.asm ff.o
lib32=$([[ -d /usr/lib32 ]] && echo /usr/lib32 || echo /lib32)
ld -m elf_i386 ${lib32}/libdl.so.2 --dynamic-linker ${lib32}/ld-linux.so.2 -s -o ff ff.o
